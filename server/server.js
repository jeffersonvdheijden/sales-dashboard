const express = require('express');
const app = express();
const port = 3001;
const axios = require('axios');

const getLatestCustomers = require('./queries/getLatestCustomers');
const getLatestClosedCustomers = require('./queries/getLatestClosedCustomers');

const mysql      = require('mysql');
const connection = mysql.createConnection({
    host     : '',
    user     : '',
    password : '',
    database : ''
});

connection.connect();

const airflowCreds = {
    appId: '',
    apiToken: ''
}

let airflowAuth = {
    auth: {
        username: airflowCreds.appId,
        password: airflowCreds.apiToken
    }
}

// CORS 
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.get('/airflow-users', (req, res) => {
    axios.get('https://api.aircall.io/v1/users', airflowAuth)
        .then((response) => {
            res.send(response.data.users)
        })
        .catch((error) => {
            console.log(error);
            res.send(error);
        })
})

app.get('/airflow-calls', (req, res) => {
    const from = req.query.from;
    const to = req.query.to;
    const url = `https://api.aircall.io/v1/calls?from=${from}&to=${to}&per_page=50`;
    let allCalls = [];

    const apiRequest = (url) => (
        axios.get(url, airflowAuth)
            .then((response) => {
                allCalls = [...allCalls, ...response.data.calls];
                if (response.data.meta.next_page_link !== null) {
                    apiRequest(response.data.meta.next_page_link);
                } else {
                    res.send(allCalls);
                }
            })
            .catch((error) => {
                console.log(error);
                res.send(error);
            })
    )
    apiRequest(url);
})

app.get('/latest-customers', (req, res) => {
    connection.query(getLatestCustomers(), (error, results) => {
        if (error) {
            console.log(error);
            res.send(error);
        }
        res.send(results);
    });
})

app.get('/latest-closed', (req, res) => {
    connection.query(getLatestClosedCustomers(), (error, results) => {
        if (error) {
            console.log(error);
            res.send(error);
        }
        res.send(results);
    });
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})