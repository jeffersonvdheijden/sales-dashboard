// List all recent Leadinfo customers

const getLatestCustomers = () => (
    'SELECT ' +
        'customer_id, ' + 
        'cancelled_at, ' + 
        'scale, ' + 
        'mrr, ' + 
        'company_name, ' +
        'company.name, ' +
        'invoice_period, ' +
        'company_country, ' +
        'firstname AS account_manager ' +
    'FROM ' + 
        'customer_subscription ' + 
        'INNER JOIN customer ON customer_subscription.customer_id=customer.id ' +
        'INNER JOIN company ON customer.company_id = company.id ' +
        'INNER JOIN user ON customer.account_manager_user_id = user.id ' +
        'LEFT OUTER JOIN company_relation ON customer.company_id = company_relation.company_id_to ' +
    'WHERE ' +
        'mrr != 0 AND ' +
        'cancelled_at != "" AND ' +
        'customer_id not in (1206, 5291, 230, 2310) ' +
        
    'ORDER BY cancelled_at DESC ' + 
    'LIMIT 5'
)

module.exports = getLatestCustomers;