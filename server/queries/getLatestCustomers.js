// List all recent Leadinfo customers

const getLatestCustomers = () => (
    'SELECT ' +
        'customer_id, ' + 
        'started_at, ' + 
        'started_at, ' + 
        'scale, ' + 
        'mrr, ' + 
        'company_name, ' +
        'company.name, ' +
        'invoice_period, ' +
        'company_country, ' +
        'account_manager_user_id, ' +
        'firstname AS account_manager ' +
    'FROM ' + 
        'customer_subscription ' + 
        'INNER JOIN customer ON customer_subscription.customer_id=customer.id ' +
        'INNER JOIN company ON customer.company_id = company.id ' +
        'INNER JOIN user ON customer.account_manager_user_id = user.id ' +
        'LEFT OUTER JOIN company_relation ON customer.company_id = company_relation.company_id_to ' +
    'WHERE ' +
        'mrr != 0 AND ' +
        'started_at != "" AND ' +
        'customer_subscription.active = 1 AND ' + 
        'expires_at is null AND ' +
        'customer_id not in (1206, 5291, 230, 2310) ' +
        
    'ORDER BY started_at DESC ' + 
    'LIMIT 5'
)

module.exports = getLatestCustomers;