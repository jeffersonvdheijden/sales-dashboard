import { combineReducers } from 'redux';

import dashboard from './../../modules/Dashboard/reducers';

const rootReducer = combineReducers({
    dashboard
});

export default rootReducer;