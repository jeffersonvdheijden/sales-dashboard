import { all } from 'redux-saga/effects';

import dashboard from './../../modules/Dashboard/watchers';

export function* rootSaga() {
    const combinedWatchers = [
        dashboard
    ];
    yield all(combinedWatchers.map(v => v()));
}
