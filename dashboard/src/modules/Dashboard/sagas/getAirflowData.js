import { put, call } from 'redux-saga/effects';
import constants from './../constants';
import { Statuses } from './../../../helpers/constants/loadingStatus';
import { airflowCallsGet } from './endpoints/airflowCalls';
import { airflowUsersGet } from './endpoints/airflowUsers';


const getAirflowData = function* () {
    try {
        yield put({ type: constants.SET_DASHBOARD_STATUS, payload: Statuses.PENDING });

        const users = yield call(airflowUsersGet, {});
        const calls = yield call(airflowCallsGet, {});

        const airflowData = {
            users: users,
            calls: calls
        }

        yield put({type: constants.SET_AIRFLOW_DATA, payload: airflowData});

        yield put({ type: constants.SET_DASHBOARD_STATUS, payload: Statuses.DONE });
        
    } catch (err) {
        console.log(err);
        yield put({ type: constants.SET_DASHBOARD_STATUS, payload: Statuses.ERROR });
    }
}

export { getAirflowData };