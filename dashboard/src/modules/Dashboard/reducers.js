import constants from './constants';
import { Statuses } from './../../helpers/constants/loadingStatus';

const initialState = {
    customerData: {
        status: Statuses.EMPTY,
        latest: []
    },
    airflowData: {
        status: Statuses.EMPTY,
        users: [],
        calls: []
    }
}

const dashboard = (state = initialState, action) => {
    switch (action.type) {
        case constants.SET_DASHBOARD_STATUS:
            return {
                ...state,
                status: action.payload
            }
        case constants.SET_AIRFLOW_DATA:
            return {
                ...state,
                airflowData: {
                    status: state.airflowData.status,
                    ...action.payload
                }
            }
        case constants.SET_CUSTOMER_DATA:
            return {
                ...state,
                customerData: {
                    status: state.customerData.status,
                    ...action.payload
                }
            }
        default:
            return state;
    }
};

export default dashboard;
