import constants from './constants';

const actions = {
    getAirflowData: () => {
        return {
            type: constants.GET_AIRFLOW_DATA
        }
    },
    getCustomerData: () => {
        return {
            type: constants.GET_CUSTOMER_DATA
        }
    }
}

export default actions;