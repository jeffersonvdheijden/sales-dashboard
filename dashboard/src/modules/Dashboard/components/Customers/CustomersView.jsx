import React from 'react';
import * as styles from './CustomersView.module.scss';

const CustomersView = ({
    latestCustomers,
    latestClosed,
    mrrGain,
    mrrLoss
}) => {
    return (
        <div className={'col-md-12'}>
            <div className={styles.title}>
                <h1>Latest clients</h1>
                <div>
                    <div className={styles.gainTitle}>Total MRR gained today:</div>
                    <div className={styles.mrrGain}>€ {mrrGain}</div>
                </div>
            </div>
            {latestCustomers.map(customer => (
                <div key={customer.customer_id} className={styles.customer}>
                    <div className={styles.time}>
                        {new Date().setHours(0,0,0,0) === new Date(customer.started_at.split('T')[0]).setHours(0,0,0,0) ? 'today' : customer.started_at.split('T')[0]} {customer.started_at.split('T')[1].split('.')[0].slice(0, -3)}
                    </div>
                    <div className={styles.customerInner}>
                        <div className={styles.customerName}>
                            {customer.company_name}
                        </div>
                        <div className={styles.accountManager}>
                            {customer.account_manager}
                        </div>
                        <div className={styles.period}>
                            {customer.invoice_period} {customer.invoice_period === 1 ? 'month' : 'months'}
                        </div>
                        <div className={styles.positiveMrr}>
                            € {customer.mrr}
                        </div>
                    </div>
                </div>
            ))}

            <br /> <br /> <br />

            <div className={styles.title}>
                <h1>Latest lost</h1>
                <div>
                    <div className={styles.lossTitle}>Total MRR lost today:</div>
                    <div className={styles.mrrLoss}>€ {mrrLoss}</div>
                </div>
            </div>
            {latestClosed.map(customer => (
                <div key={customer.customer_id} className={styles.customer}>
                    <div className={styles.time}>
                    {new Date().setHours(0,0,0,0) === new Date(customer.cancelled_at.split('T')[0]).setHours(0,0,0,0) ? 'today' : customer.cancelled_at.split('T')[0]} {customer.cancelled_at.split('T')[1].split('.')[0].slice(0, -3)}
                    </div>
                    <div className={styles.customerInner}>
                        <div className={styles.customerName}>
                            {customer.company_name}
                        </div>
                        <div className={styles.accountManager}>
                            {customer.account_manager}
                        </div>
                        <div className={styles.period}>
                            {customer.invoice_period} {customer.invoice_period === 1 ? 'month' : 'months'}
                        </div>
                        <div className={styles.negativeMrr}>
                            € {customer.mrr}
                        </div>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default CustomersView;