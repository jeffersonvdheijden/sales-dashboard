import React from 'react';
import * as styles from './AirflowView.module.scss';

const AirflowView = ({
    userCalls,
    totalCalls,
    totalCallTime
}) => {
    return (
        <div className={styles.airflow}>
            <div className={'col-md-3'}>
                <h1 className={styles.title}>Calls</h1>
                <ul>
                    {userCalls && userCalls.map((user, i) => (
                        <li key={user.id}>
                            <span>{i === 0 && ( <>⭐</> )} {user.name.split(' ')[0]}</span>
                            <span>{user.calls.length}</span>
                        </li>
                    ))}
                    <li>
                        <span className={styles.total}>Total</span>
                        <span>{totalCalls}</span>
                    </li>
                </ul>
            </div>
            <div className={'col-md-3'}>
                <h1 className={styles.title}>Call duration</h1>
                <ul>
                    {userCalls && userCalls.map((user, i) => (
                        <li key={user.id}>
                            <span>{i === 0 && ( <>⭐</> )} {user.name.split(' ')[0]}</span> 
                            <span>{new Date(user.total_call_duration * 1000).toISOString().substr(11, 8)}</span>
                        </li>
                    ))}
                    <li>
                        <span className={styles.total}>Total</span>
                        <span>{new Date(totalCallTime * 1000).toISOString().substr(11, 8)}</span>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default AirflowView;