import React, { useState } from 'react';
import * as styles from './Clock.module.scss';
import useInterval from './../../../../helpers/hooks/useInterval';

const Clock = () => {
    const [time, setTime] = useState(new Date());

    useInterval(() => {
        setTime(new Date());
    }, 1000);

    const h = time.getHours();
	const m = time.getMinutes();
    const s = time.getSeconds();
    const date = time.toDateString();

    return (
        <h1 className={styles.clock}>
            {date} - {h}:{(m < 10 ? '0' + m : m)}:{(s < 10 ? '0' + s : s)}
        </h1>
    )
}

export default Clock;