import React from 'react';
import * as styles from './DashboardView.module.scss';

import LeadinfoLogo from './LeadinfoLogo';
import Clock from './../Clock/Clock';
import AirflowView from './../Airflow/AirflowView';
import CustomersView from './../Customers/CustomersView';

const DashboardView = ({
    userCalls,
    latestCustomers,
    latestClosed,
    mrrGain,
    mrrLoss,
    totalCalls,
    totalCallTime
}) => {
    return (
        <div className={styles.dashboard}>
            <div className={'container'}>
                <div className={'row'}>

                    <div className={'col-md-12'}>
                        <div className={styles.header}>
                            <LeadinfoLogo />
                            <Clock />
                        </div>
                    </div>
                    
                    {latestCustomers.length > 0 && (
                        <CustomersView 
                            latestCustomers={latestCustomers} 
                            latestClosed={latestClosed}
                            mrrGain={mrrGain}
                            mrrLoss={mrrLoss}
                        />
                    )}

                    {userCalls.length > 0 && (
                        <AirflowView 
                            userCalls={userCalls}
                            totalCalls={totalCalls}
                            totalCallTime={totalCallTime}
                        />      
                    )}

                </div>
            </div>
        </div>
    )
}

export default DashboardView;