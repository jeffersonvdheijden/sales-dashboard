import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useInterval from './../../../../helpers/hooks/useInterval';
import actions from '../../actions';
import * as selectors from './../../selectors';

import DashboardView from './DashboardView';

const Dashboard = () => {
    const dispatch = useDispatch();
    const latestCustomers   = useSelector(state => state.dashboard.customerData.latest);
    const latestClosed      = useSelector(state => state.dashboard.customerData.closed);
    const callDataPerUser   = useSelector(state => selectors.callDataPerUser(state));
    const mrrGain           = useSelector(state => selectors.mrrGain(state));
    const mrrLoss           = useSelector(state => selectors.mrrLoss(state));
    const totalCalls        = useSelector(state => selectors.totalCalls(state));
    const totalCallTime     = useSelector(state => selectors.totalCallTime(state));

    useEffect(() => {
        dispatch(actions.getAirflowData());
        dispatch(actions.getCustomerData());
    }, [dispatch]);

    // Custom hook, fetches data every 5 minutes
    useInterval(() => {
        dispatch(actions.getAirflowData());
        dispatch(actions.getCustomerData());
    }, 300000);

    return (
        <DashboardView
            userCalls={callDataPerUser}
            latestCustomers={latestCustomers}
            latestClosed={latestClosed}
            mrrGain={mrrGain}
            mrrLoss={mrrLoss}
            totalCalls={totalCalls}
            totalCallTime={totalCallTime}
        />
    )
}

export default Dashboard;