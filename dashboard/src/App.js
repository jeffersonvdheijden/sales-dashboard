import { Provider } from 'react-redux';
import store from './helpers/state/store';
import './helpers/styles/reset.scss';
import './helpers/styles/grid.scss';
import './helpers/styles/colors.scss'

import Dashboard from './modules/Dashboard/components/Dashboard/Dashboard';

function App() {
  return (
    <Provider store={store}>
      <Dashboard />
    </Provider>
  );
}

export default App;